package zheshao.imagedownload.test;

import android.app.Application;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by dengdayi
 * Date: 2016/10/08 16：10
 */
public class MyApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        initConfigImageLoader();
    }

    private void initConfigImageLoader(){
        ImageLoaderConfiguration.Builder config=new ImageLoaderConfiguration.Builder(getApplicationContext());
//        config.threadPriority(Thread.NORM_PRIORITY - 2);
//        config.denyCacheImageMultipleSizesInMemory();
//        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
//        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
//        config.tasksProcessingOrder(QueueProcessingType.LIFO);
//        config.writeDebugLogs(); // Remove for release app

        ImageLoader.getInstance().init(config.build());
    }
}
