package zheshao.imagedownload.test;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    private static final String IMAGE_URL = "http://nuuneoi.com/uploads/source/playstore/cover.jpg";

    private ImageView mIvImageLoad, mIvGlide, mIvPicasso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mIvGlide = (ImageView) findViewById(R.id.iv_glide);
        mIvPicasso = (ImageView) findViewById(R.id.iv_picasso);
        mIvImageLoad = (ImageView) findViewById(R.id.iv_imageLoad);
    }

    /**
     * Glide点击事件
     *
     * @param view
     */
    public void onGlideLoad(View view) {
        Glide.with(this).load(IMAGE_URL).into(mIvGlide);
    }

    /**
     * Picasso点击事件
     *
     * @param view
     */
    public void onPicassoLoad(View view) {
        Picasso.with(this).load(IMAGE_URL).into(mIvPicasso);
    }

    /**
     * ImageLoader点击事件
     *
     * @param view
     */
    public void onImageLoad(View view) {
        ImageLoader.getInstance().displayImage(IMAGE_URL, mIvImageLoad);
    }
}
